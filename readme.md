-Setup your token to be semi animated like 8bit games of old. This module allows you to set a different image for each direction (WSAD, arrow keys).

-Use the convenient TokenHud setup or the Token configuration screen to pick the images.

-Lock the setup and forget about it. And enjoy as your tokens now face the direction you want them to go.

-See CHANGELOG for the latest changes.


#**I will no longer be updating this module, it was written as a dare and I just dont have time to maintain it anymore.**